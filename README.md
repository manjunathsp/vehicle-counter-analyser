# Vehicle counter analyser

## Why I choose this problem ?

Of the two problems that was presented I choose as I found this to be an interesting data analysis 
problem solve. I was also curious to know what the output of this data would be.

## Design 

### Data ingestion

In this coding challenge I am reading the file from the resources folder or a file could be supplied.

I parse each line of data into a POJO VehicleRecord. VehicleRecordParser class has logic to parse this file.
The VehicleRecord has day, direction, and the front axel timestamp and the rear axel timestamp.

The parser throws an exception if the records to meet the requirements. At this time parser is only 
capable of handling the inputs in the given format. 


### Data Analysis

VehicleRecordAnalysisService has methods to analyse the VehicleRecords. I used TreeMap in order to
keep the ordering of results based on day.

### Report Generation

I found this part to be tricky. I am writing the results to the console (I could have create a CSV 
file for each report and thought that it can be extend later on). Since we wanted reports to be based 
on sessions. I tried to make it extensible by passing the session to each generator.

Also the format for each of the session varied, so ended up creating interfaces for each of the session.
Report generator is capable of producing reports for different periods.


## Running the application

Check out the code using:

`git clone https://manjunathsp@bitbucket.org/manjunathsp/vehicle-counter-analyser.git`

Navigate to the 'vehicle-counter-analyser' directory under the cloned repository from above and run:

`mvn clean install`

This will run all tests and generate the jar at below location:

`/vehicle-counter-analyser/target/vehicle-counter-analyser-1.0-SNAPSHOT.jar`

### Executing the application

#### By passing a file to the application

`java -jar target/vehicle-counter-analyser-1.0-SNAPSHOT.jar /usr/data/data.txt > reports.txt`

The report will be written to `reports.txt` file

#### Default

The data file supplied in the challenge will be read from resources directory

`java -jar target/vehicle-counter-analyser-1.0-SNAPSHOT.jar > reports.txt`

The report will be written to `reports.txt` file