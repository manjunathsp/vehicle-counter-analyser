package org.vehicle.survey.counter.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility method to read and write files.
 */
public class FileUtils {

  private static final Logger LOGGER = Logger.getLogger(FileUtils.class.getName());

  /**
   * Prevent initialization of utility class
   */
  private FileUtils() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Reads a file to a List
   *
   * @param filePath - the file path
   * @return - an empty list if file is empty or cannot be read, else file contents as a list
   */
  public static List<String> readFileAsList(Path filePath) {

    List<String> fileContents = new ArrayList<>();
    if (filePath != null) {
      try (Stream<String> stream = Files.lines(filePath)) {

        fileContents.addAll(stream.collect(Collectors.toList()));

      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, e.getMessage(), e);
      }
    } else {
      LOGGER.log(Level.SEVERE, "the path is NULL, check if file exists");
    }
    return fileContents;
  }


  /**
   * Read file as stream and return list of values
   */
  public static List<String> readFileAsList(final String filePath) {
    LOGGER.log(Level.INFO, String.format("Processing file: %s", filePath));
    List<String> fileContents = new ArrayList<>();
    try {
      try (Scanner scanner = new Scanner(new FileInputStream(filePath))) {
        while (scanner.hasNext()) {
          fileContents.add(scanner.nextLine().trim());
        }
      }
    } catch (FileNotFoundException e) {
      LOGGER.log(Level.SEVERE,
          String.format("Skipping processing \"%s\" as the file couldn't be located.", filePath));
    }
    return fileContents;
  }
}
