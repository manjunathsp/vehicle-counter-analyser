package org.vehicle.survey.counter.analysis.report.peak;

import java.time.Duration;

/**
 * Peak volume times.
 */
public class PeakHourVolumeTimesReportGenerator
    implements PeakVolumeTimesReportGenerator {

  @Override
  public String getReportTitle() {
    return "Peak volume times";
  }


  @Override
  public long getInterval() {
    return Duration.ofHours(1).toMillis();
  }
}
