package org.vehicle.survey.counter.analysis;


import java.util.List;
import java.util.Map;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;
import org.vehicle.survey.counter.model.VehicleRecord;

/**
 * Interface with methods for analysing data
 */
public interface VehicleRecordAnalysisService {


  /**
   * To compute the vehicle count
   *
   * @param direction - the direction
   * @param start - the start time in millis
   * @param end - the end time in millis
   * @return - A map of day to vehicle count based on direction
   */
  Map<Integer, List<VehicleRecord>> findVehicleCount(VehicleDirectionEnum direction, long start,
      long end);

  /**
   * To find the peak volume for a period length for a direction
   *
   * @param duration - the period duration in millis
   * @param direction - the direction
   * @return - the result
   */
  PeakVolumeResult findPeakVolume(long duration, VehicleDirectionEnum direction);

  /**
   * To compute average number of vehicles
   *
   * @param duration - the duration in millis
   * @param direction - the direction
   * @return - map with period start timestamp
   */
  Map<Long, AverageCountResult> getCountAverages(long duration, VehicleDirectionEnum direction);

  /**
   * To compute average speed of vehicles
   *
   * @param duration - the  duration in millis
   * @param direction - the direction
   * @return Map with entry period start time timestamp since day start, faster access with this key
   */
  Map<Long, AverageCountResult> getSpeedAverages(long duration, VehicleDirectionEnum direction);

  /**
   * To compute average distance in meters between vehicles
   *
   * @param duration - the duration in millis
   * @param direction - the direction
   * @return Map with entry period start time timestamp since day start, faster access with this key
   */
  Map<Long, AverageCountResult> getDistanceAverages(long duration,
      VehicleDirectionEnum direction);

  /**
   * Returns a list of days based on vehicle reocrds
   *
   * @return - a list of unique values
   */
  List<Integer> getRecordedDays();


}
