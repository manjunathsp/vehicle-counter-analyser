package org.vehicle.survey.counter.analysis.report.count;

import java.time.Duration;

/**
 * Total vehicle counts in each direction: per 20 minutes
 */
public class VehicleCountPerTwentyMinutesReport
    implements VehicleCountReportGenerator {

  @Override
  public String getReportTitle() {
    return "Total vehicle counts in each direction: per 20 minutes";
  }


  @Override
  public long getInterval() {
    return Duration.ofMinutes(20).toMillis();
  }

}
