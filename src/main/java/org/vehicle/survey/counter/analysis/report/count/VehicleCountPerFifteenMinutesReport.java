package org.vehicle.survey.counter.analysis.report.count;

import java.time.Duration;

/**
 * Total vehicle counts in each direction: per 15 minutes
 */
public class VehicleCountPerFifteenMinutesReport
    implements VehicleCountReportGenerator {


  @Override
  public String getReportTitle() {
    return "Total vehicle counts in each direction: per 15 minutes.";
  }


  @Override
  public long getInterval() {
    return Duration.ofMinutes(15).toMillis();
  }

}
