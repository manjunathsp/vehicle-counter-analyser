package org.vehicle.survey.counter.analysis;

/**
 * Class to represent the average
 */
public class AverageCountResult {

  private long hour;

  private float average;

  public AverageCountResult(long hour, float average) {
    this.hour = hour;
    this.average = average;
  }

  public long getHour() {
    return this.hour;
  }

  public void setHour(long hour) {
    this.hour = hour;
  }

  public float getAverage() {
    return this.average;
  }

  public void setAverage(float average) {
    this.average = average;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("AverageResult{");
    sb.append("hour=").append(this.hour);
    sb.append(", average=").append(this.average);
    sb.append('}');
    return sb.toString();
  }
}
