package org.vehicle.survey.counter.analysis.report;


import java.time.Duration;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.vehicle.survey.counter.analysis.AverageCountResult;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;

/**
 * Abstract class to generate the report
 */
public interface ReportGenerator {

  /**
   * The report title
   */
  String getReportTitle();

  /**
   * Returns milliseconds in hh:mm:ss format
   *
   * @param millis - the milliseconds
   *
   * return - string in hh:mm:ss format
   */
  default String getFormattedTime(long millis) {

    return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
        TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
        TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
  }

  /**
   *
   * @param directions
   * @param interval
   * @param resultsPerDirection
   */
  default void displayReport(VehicleDirectionEnum[] directions, long interval,
      Map<VehicleDirectionEnum, Map<Long, AverageCountResult>> resultsPerDirection) {
    for (long currentTime = 0; currentTime < Duration.ofDays(1).toMillis();
        currentTime += interval) {
      System.out.println("");

      System.out.print(this.getFormattedTime(currentTime) + " \t");

      for (VehicleDirectionEnum direction : directions) {
        AverageCountResult averageResult = resultsPerDirection.get(direction)
            .get(Long.valueOf(currentTime));

        System.out.print("\t\t\t\t" + averageResult.getAverage() + "\t\t\t\t");
      }
      System.out.println("");
    }
  }
}
