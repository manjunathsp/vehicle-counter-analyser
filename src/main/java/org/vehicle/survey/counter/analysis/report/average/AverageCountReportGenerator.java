package org.vehicle.survey.counter.analysis.report.average;

import java.util.Map;
import java.util.TreeMap;
import org.vehicle.survey.counter.analysis.AverageCountResult;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.analysis.report.ReportGenerator;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;

/**
 * Average vehicle count for each day of the session, or you can see averages across all the days.
 */
public interface AverageCountReportGenerator
    extends ReportGenerator {

  /**
   * Display the report
   */
  default void displayReport(VehicleRecordAnalysisService vehicleRecordAnalysisService) {
    VehicleDirectionEnum[] directions = VehicleDirectionEnum.values();
    long interval = this.getInterval();

    System.out.print("Hour\t\t\t\t\t");

    Map<VehicleDirectionEnum, Map<Long, AverageCountResult>> resultsPerDirection = new TreeMap<>();

    for (VehicleDirectionEnum direction : directions) {
      System.out.print("Direction " + direction + "\t\t");

      resultsPerDirection
          .put(direction, vehicleRecordAnalysisService.getCountAverages(interval, direction));
    }

    this.displayReport(directions, interval, resultsPerDirection);

  }

  /**
   * the time interval
   */
  long getInterval();

}
