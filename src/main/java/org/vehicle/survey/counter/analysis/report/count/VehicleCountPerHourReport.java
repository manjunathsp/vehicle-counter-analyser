package org.vehicle.survey.counter.analysis.report.count;

import java.time.Duration;

/**
 * Total vehicle counts in each direction: per hour
 */
public class VehicleCountPerHourReport
    implements VehicleCountReportGenerator {


  @Override
  public String getReportTitle() {
    return "Total vehicle counts in each direction: per hour";
  }


  @Override
  public long getInterval() {
    return Duration.ofMinutes(60).toMillis();
  }
}