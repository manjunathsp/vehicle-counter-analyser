package org.vehicle.survey.counter.analysis.report.speed;

import java.time.Duration;

/**
 * The (rough) speed distribution of traffic.
 */
public class SpeedDistributionReportGeneratorByHourReport
    extends SpeedDistributionReportGenerator {

  @Override
  public long getInterval() {
    return Duration.ofHours(1).toMillis();
  }

  @Override
  public String getReportTitle() {
    return "The (rough) average speed distribution of traffic in kmph";
  }
}
