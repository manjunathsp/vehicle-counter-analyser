package org.vehicle.survey.counter.analysis.report.distance;

import java.time.Duration;

/**
 * Rough distance between cars (in meters) during various periods(per hour)
 */
public class AverageDistanceBetweenVehiclesReport
    implements AverageDistanceBetweenVehiclesReportGenerator {

  @Override
  public long getInterval() {
    return Duration.ofHours(1).toMillis();
  }

  @Override
  public String getReportTitle() {
    return "Rough distance between cars (in meters) during various periods";
  }
}
