package org.vehicle.survey.counter.analysis.report.average;

import java.time.Duration;

/**
 * Average vehicle counts in each direction for each day of the session
 */
public class VehicleAverageCountPerHour
    implements AverageCountReportGenerator {

  @Override
  public long getInterval() {
    return Duration.ofHours(1).toMillis();
  }

  @Override
  public String getReportTitle() {
    return "Average vehicle counts in each direction for each day of the session";
  }

}
