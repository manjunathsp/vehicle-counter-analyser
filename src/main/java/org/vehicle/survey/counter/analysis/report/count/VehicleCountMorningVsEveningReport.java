package org.vehicle.survey.counter.analysis.report.count;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.analysis.report.ReportGenerator;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;
import org.vehicle.survey.counter.model.VehicleRecord;

/**
 * Total vehicle counts in each direction: morning versus evening
 */
public class VehicleCountMorningVsEveningReport implements ReportGenerator {


  @Override
  public String getReportTitle() {
    return "Total vehicle counts in each direction: morning versus evening";
  }

  /**
   * Display the report
   */
  public void displayReport(VehicleRecordAnalysisService vehicleRecordAnalysisService) {

    System.out.println(this.getReportTitle());
    System.out.println("----------------------------------------");
    List<Integer> days = vehicleRecordAnalysisService.getRecordedDays();
    VehicleDirectionEnum[] directions = VehicleDirectionEnum.values();

    for (VehicleDirectionEnum direction : directions) {

      // morning first and evening
      Map<Integer, List<VehicleRecord>> vehiclesBeforeNoonByDay =
          vehicleRecordAnalysisService
              .findVehicleCount(direction, 0, Duration.ofHours(12).toMillis());
      Map<Integer, List<VehicleRecord>> vehiclesAfterNoonByDay =
          vehicleRecordAnalysisService
              .findVehicleCount(direction, Duration.ofHours(12).toMillis(),
                  Duration.ofDays(1).toMillis());

      System.out.println("Vehicles direction " + direction);
      String seprator = "_______________________________________";
      System.out.println(seprator);
      System.out.println("Day\t Morning Count\t Evening Count");
      System.out.println(seprator);

      for (Integer day : days) {
        List<VehicleRecord> morning = vehiclesBeforeNoonByDay.get(day);
        int morningCount = morning == null ? 0 : morning.size();
        List<VehicleRecord> evening = vehiclesAfterNoonByDay.get(day);
        int eveningCount = evening == null ? 0 : evening.size();
        System.out.println(
            day.toString() + "\t\t\t " + morningCount + "\t\t\t " + eveningCount);
      }

      System.out.println(seprator);
      System.out.println("");

    }


  }


}
