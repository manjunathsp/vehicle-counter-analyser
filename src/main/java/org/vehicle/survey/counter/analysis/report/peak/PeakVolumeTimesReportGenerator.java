package org.vehicle.survey.counter.analysis.report.peak;

import org.vehicle.survey.counter.analysis.PeakVolumeResult;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.analysis.report.ReportGenerator;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;

/**
 * Peak volume times.
 */
public interface PeakVolumeTimesReportGenerator extends ReportGenerator {

  /**
   * Display the report
   */
  default void displayReport(VehicleRecordAnalysisService vehicleRecordAnalysisService) {
    VehicleDirectionEnum[] directions = VehicleDirectionEnum.values();
    long increment = this.getInterval();

    System.out.println("");
    System.out.println("Report: " + this.getReportTitle());
    System.out.println("_____________________________________________________________");

    for (VehicleDirectionEnum direction : directions) {
      PeakVolumeResult peakVolume = vehicleRecordAnalysisService
          .findPeakVolume(increment, direction);
      System.out.println(
          "Found peak volume for direction " + direction + ": " + peakVolume.getNumber() + ", day "
              + peakVolume.getDay()
              + " at " + this.getFormattedTime(peakVolume.getHour()));
    }

    System.out.println("_____________________________________________________________");
  }


  long getInterval();


}
