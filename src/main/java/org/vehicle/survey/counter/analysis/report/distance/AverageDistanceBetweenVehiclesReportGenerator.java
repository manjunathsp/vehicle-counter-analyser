package org.vehicle.survey.counter.analysis.report.distance;

import java.util.Map;
import java.util.TreeMap;
import org.vehicle.survey.counter.analysis.AverageCountResult;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.analysis.report.ReportGenerator;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;

/**
 * Rough distance between cars during various periods
 */
public interface AverageDistanceBetweenVehiclesReportGenerator extends ReportGenerator {

  /**
   * Displays the report
   */
  default void displayReport(VehicleRecordAnalysisService vehicleRecordAnalysisService) {
    VehicleDirectionEnum[] directions = VehicleDirectionEnum.values();
    long interval = this.getInterval();

    System.out.println("");
    System.out.println("Report: " + this.getReportTitle());
    System.out.println("________________________________________________________");

    System.out.print("Hour\t\t\t\t\t");

    Map<VehicleDirectionEnum, Map<Long, AverageCountResult>> resultsPerDirection = new TreeMap<>();

    for (VehicleDirectionEnum direction : directions) {
      System.out.print("Direction " + direction + "\t\t\t\t\t\t");

      resultsPerDirection
          .put(direction, vehicleRecordAnalysisService.getDistanceAverages(interval, direction));
    }

    this.displayReport(directions, interval, resultsPerDirection);
  }

  /**
   * Get the time interval in milliseconds
   */
  long getInterval();

}
