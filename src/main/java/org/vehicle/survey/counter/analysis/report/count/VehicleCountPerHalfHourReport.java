package org.vehicle.survey.counter.analysis.report.count;

import java.time.Duration;

/**
 * Total vehicle counts in each direction per half hour
 */
public class VehicleCountPerHalfHourReport
    implements VehicleCountReportGenerator {


  @Override
  public String getReportTitle() {
    return "Total vehicle counts in each direction per half hour";
  }


  @Override
  public long getInterval() {
    return Duration.ofMinutes(30).toMillis();
  }

}
