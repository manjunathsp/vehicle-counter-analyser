package org.vehicle.survey.counter.analysis.report.count;


import java.time.Duration;
import java.util.List;
import java.util.Map;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.analysis.report.ReportGenerator;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;
import org.vehicle.survey.counter.model.VehicleRecord;

/**
 * Base class for reports: Total vehicle counts in each direction: morning versus evening, per hour,
 * per half hour, per 20 minutes, and per 15 minutes.
 */
public interface VehicleCountReportGenerator extends ReportGenerator {

  /**
   * The reporting interval in milliseconds
   */
  long getInterval();

  /**
   * Display the report based on the interval set
   */
  default void displayReport(VehicleRecordAnalysisService vehicleRecordAnalysisService) {
    VehicleDirectionEnum[] directions = VehicleDirectionEnum.values();
    List<Integer> days = vehicleRecordAnalysisService.getRecordedDays();

    long interval = this.getInterval();

    System.out.println("");
    System.out.println("Report: " + this.getReportTitle());

    for (Integer day : days) {
      System.out.println("Day " + day);
      System.out.println("_______________________________________");
      System.out.print("Hour\t\t\t\t\t");

      for (VehicleDirectionEnum direction : directions) {
        System.out.print("Direction " + direction + "\t\t");
      }

      System.out.println("");
      for (long currentTime = 0; currentTime < Duration.ofDays(1).toMillis();
          currentTime += interval) {
        System.out.print(this.getFormattedTime(currentTime) + " \t");

        for (VehicleDirectionEnum direction : directions) {
          Map<Integer, List<VehicleRecord>> found =
              vehicleRecordAnalysisService.findVehicleCount(direction, currentTime,
                  currentTime + interval - 1);
          List<VehicleRecord> vehiculeRecords = found.get(day);
          int number = vehiculeRecords == null ? 0 : vehiculeRecords.size();
          System.out.print("\t\t\t\t" + number + "\t\t\t\t");

        }
        System.out.println("");
      }
      System.out.println("_______________________________________");

    }

  }

}
