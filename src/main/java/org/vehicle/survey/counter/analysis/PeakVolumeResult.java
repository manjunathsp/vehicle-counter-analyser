package org.vehicle.survey.counter.analysis;

/**
 * Class to represent peak traffic times
 */
public class PeakVolumeResult {

  private int day;

  private long hour;

  private int number;

  public PeakVolumeResult(int day, long hour, int number) {
    this.day = day;
    this.hour = hour;
    this.number = number;
  }

  public int getDay() {
    return this.day;
  }

  public void setDay(int day) {
    this.day = day;
  }

  public long getHour() {
    return this.hour;
  }

  public void setHour(long hour) {
    this.hour = hour;
  }

  public int getNumber() {
    return this.number;
  }

  public void setNumber(int number) {
    this.number = number;
  }

  @Override
  public String toString() {
    return "PeakVolumeResult{" +
        "day=" + this.day +
        ", hour=" + this.hour +
        ", number=" + this.number +
        '}';
  }
}