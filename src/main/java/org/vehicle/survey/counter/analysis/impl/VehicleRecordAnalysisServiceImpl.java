package org.vehicle.survey.counter.analysis.impl;

import static org.vehicle.survey.counter.config.ApplicationConfig.WHEEL_BASE_SIZE;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.vehicle.survey.counter.analysis.AverageCountResult;
import org.vehicle.survey.counter.analysis.PeakVolumeResult;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.config.ApplicationConfig;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;
import org.vehicle.survey.counter.model.VehicleRecord;

/**
 * Implementation class for analysis.
 */
public class VehicleRecordAnalysisServiceImpl implements VehicleRecordAnalysisService {

  /**
   * The vehicle records
   */
  private final List<VehicleRecord> vehicleRecords = new ArrayList<>();

  /**
   * The days for which records have been parsed
   */
  private final List<Integer> recordedDays = new ArrayList<>();

  /**
   * Constructor, keep the records in-memory
   *
   * @param vehicleRecords - a list of vehicleRecords to be analysed
   */
  public VehicleRecordAnalysisServiceImpl(List<VehicleRecord> vehicleRecords) {
    this.vehicleRecords.addAll(vehicleRecords);

    this.recordedDays.addAll(vehicleRecords.stream().map(
        VehicleRecord::getDay).distinct().collect(Collectors.toList()));
  }

  @Override
  public Map<Integer, List<VehicleRecord>> findVehicleCount(VehicleDirectionEnum direction,
      long start, long end) {

    return this.vehicleRecords
        .parallelStream().filter(vehicleRecord -> vehicleRecord.getDirection().equals(direction)
            && vehicleRecord.getFirstAxelTimestamp() >= start
            && vehicleRecord.getFirstAxelTimestamp() < end)
        .collect(sortedGroupingBy(VehicleRecord::getDay));

  }

  @Override
  public PeakVolumeResult findPeakVolume(long duration,
      VehicleDirectionEnum direction) {
    PeakVolumeResult peakVolumeResult = new PeakVolumeResult(0, 0, 0);
    for (Integer day : this.recordedDays) {

      for (long currentTime = 0; currentTime < Duration.ofDays(1).toMillis();
          currentTime += duration) {
        Map<Integer, List<VehicleRecord>> filteredRecords =
            this.findVehicleCount(direction, currentTime, currentTime + duration - 1);
        List<VehicleRecord> vehicleRecordsByDay = filteredRecords.get(day);
        int number = vehicleRecordsByDay == null ? 0 : vehicleRecordsByDay.size();
        if (number > peakVolumeResult.getNumber() && vehicleRecordsByDay != null) {
          peakVolumeResult = new PeakVolumeResult(vehicleRecordsByDay.get(0).getDay(), currentTime,
              number);
        }
      }
    }
    return peakVolumeResult;
  }


  @Override
  public Map<Long, AverageCountResult> getCountAverages(long duration,
      VehicleDirectionEnum direction) {
    Map<Long, AverageCountResult> averageResults = new TreeMap<>();

    for (long currentTime = 0; currentTime < Duration.ofDays(1).toMillis();
        currentTime += duration) {

      Map<Integer, List<VehicleRecord>> filteredMap =
          this.findVehicleCount(direction, currentTime, currentTime + duration - 1);
      AtomicLong sum = new AtomicLong();
      filteredMap.forEach((key, value) ->
          sum.addAndGet(value.size())
      );

      averageResults
          .put(Long.valueOf(currentTime),
              new AverageCountResult(currentTime, sum.get() / (float) this.recordedDays
                  .size()));

    }
    return averageResults;
  }

  @Override
  public Map<Long, AverageCountResult> getSpeedAverages(long duration,
      VehicleDirectionEnum direction) {
    Map<Long, AverageCountResult> averageResults = new TreeMap<>();

    for (long currentTime = 0; currentTime < Duration.ofDays(1).toMillis();
        currentTime += duration) {

      Map<Integer, List<VehicleRecord>> filteredMap =
          this.findVehicleCount(direction, currentTime, currentTime + duration - 1);
      float totalSpeed = 0;
      int totalRecords = 0;

      for (Map.Entry<Integer, List<VehicleRecord>> entry : filteredMap.entrySet()) {
        for (VehicleRecord record : entry.getValue()) {
          long time =
              record.getSecondAxelTimestamp() - record.getFirstAxelTimestamp();
          totalSpeed += ((WHEEL_BASE_SIZE / 1000) * Duration.ofHours(1).toMillis()) / time;
          totalRecords++;
        }
      }

      if (totalRecords == 0) {
        //Avg set to 0
        averageResults.put(Long.valueOf(currentTime), new AverageCountResult(currentTime, 0));
      } else {
        averageResults.put(Long.valueOf(currentTime),
            new AverageCountResult(currentTime, totalSpeed / totalRecords));
      }
    }
    return averageResults;
  }


  @Override
  public Map<Long, AverageCountResult> getDistanceAverages(long duration,
      VehicleDirectionEnum direction) {

    Map<Long, AverageCountResult> averageResults = new TreeMap<>();

    for (long currentTime = 0; currentTime < Duration.ofDays(1).toMillis();
        currentTime += duration) {

      Map<Integer, List<VehicleRecord>> filteredRecords =
          this.findVehicleCount(direction, currentTime, currentTime + duration - 1);
      float totalDistance = 0;
      int totalRecords = 0;
      for (Map.Entry<Integer, List<VehicleRecord>> entry : filteredRecords.entrySet()) {
        if (entry.getValue().isEmpty()) {
          continue;
        }

        Iterator<VehicleRecord> vehicleRecordIterator = entry.getValue().iterator();
        VehicleRecord previousRecord = vehicleRecordIterator.next();
        while (vehicleRecordIterator.hasNext()) {
          VehicleRecord currentRecord = vehicleRecordIterator.next();

          long time = currentRecord.getFirstAxelTimestamp() - previousRecord
              .getFirstAxelTimestamp();

          totalDistance += (ApplicationConfig.AVERAGE_SPEED_PER_HOUR * 1000 * time)
              / Duration.ofHours(1).toMillis();

          totalRecords++;

          previousRecord = currentRecord;
        }
      }
      if (totalRecords == 0) {
        averageResults.put(Long.valueOf(currentTime), new AverageCountResult(currentTime, 0));
      } else {
        averageResults.put(Long.valueOf(currentTime),
            new AverageCountResult(currentTime, totalDistance / totalRecords));
      }

    }
    return averageResults;
  }

  /**
   * Returns a list of days
   */
  @Override
  public List<Integer> getRecordedDays() {
    return this.recordedDays;
  }

  private static <T, K extends Comparable<K>> Collector<T, ?, TreeMap<K, List<T>>>
  sortedGroupingBy(Function<T, K> function) {
    return Collectors.groupingBy(function,
        TreeMap::new, Collectors.toList());
  }

}
