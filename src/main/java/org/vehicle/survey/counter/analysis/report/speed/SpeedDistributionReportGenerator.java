package org.vehicle.survey.counter.analysis.report.speed;

import java.util.Map;
import java.util.TreeMap;
import org.vehicle.survey.counter.analysis.AverageCountResult;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.analysis.report.ReportGenerator;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;

/**
 * The (rough) speed distribution of traffic.
 */
public abstract class SpeedDistributionReportGenerator
    implements ReportGenerator {

  public void displayReport(VehicleRecordAnalysisService vehicleRecordAnalysisService) {
    VehicleDirectionEnum[] directions = VehicleDirectionEnum.values();
    long interval = this.getInterval();

    System.out.println("");
    System.out.println("Report: " + this.getReportTitle());
    System.out.println("________________________________________________________");
    System.out.print("Hour\t\t\t\t\t");

    Map<VehicleDirectionEnum, Map<Long, AverageCountResult>> resultsPerDirection =
        new TreeMap<>();

    for (VehicleDirectionEnum direction : directions) {
      System.out.print("Direction " + direction + "\t\t\t\t");

      resultsPerDirection
          .put(direction, vehicleRecordAnalysisService.getSpeedAverages(interval, direction));
    }

    this.displayReport(directions, interval, resultsPerDirection);

  }

  protected abstract long getInterval();

}
