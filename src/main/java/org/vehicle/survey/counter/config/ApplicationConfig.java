package org.vehicle.survey.counter.config;


/**
 * This has config properties which can be changed based on the input and business logic
 */
public class ApplicationConfig {

  /**
   * private constructor
   */
  private ApplicationConfig() {
    throw new IllegalStateException();
  }

  /**
   * In meters
   */
  public static final float WHEEL_BASE_SIZE = (float) 2.5;

  /**
   * In kph
   */
  public static final float AVERAGE_SPEED_PER_HOUR = (float) 60;

  /**
   * the input regex
   */
  public static final String INPUT_VALIDATION_REGEX = "(^[AaBb])(\\d+)$";

  /**
   * Sensor 1 prefix for north bound and south bound traffic
   */
  public static final String SENSOR1_PREFIX = "A";

  /**
   * Sensor 1 prefix for south bound traffic
   */
  public static final String SENSOR2_PREFIX = "B";

  /**
   * The date format to be used in the report
   */
  public static final String DATE_FORMAT = "HH:mm";

}
