package org.vehicle.survey.counter.model;

/**
 * Indicates the direction of the Vehicle
 */
public enum VehicleDirectionEnum {
  //Northbound
  A,
  //Southbound
  B
}
