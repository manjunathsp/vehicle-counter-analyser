package org.vehicle.survey.counter.model;

/**
 * A Vehicle record
 */
public class VehicleRecord {

  /**
   * Represents day number, use 1 for the 1st day
   */
  private final int day;

  /**
   * the vehicle direction
   */
  private VehicleDirectionEnum direction;

  /**
   * The first axel timestamp
   */
  private long firstAxelTimestamp;

  /**
   * The second axel timestamp
   */
  private long secondAxelTimestamp;


  /**
   * Constructor
   *
   * @param day - the day starts form 1
   * @param direction - the direction A or B
   * @param firstAxelTimestamp - the firstAxelTimestamp
   * @param secondAxelTimestamp -  the secondAxelTimestamp
   */
  public VehicleRecord(int day, VehicleDirectionEnum direction, long firstAxelTimestamp,
      long secondAxelTimestamp) {
    this.day = day;
    this.direction = direction;
    this.firstAxelTimestamp = firstAxelTimestamp;
    this.secondAxelTimestamp = secondAxelTimestamp;
  }

  public int getDay() {
    return this.day;
  }

  public VehicleDirectionEnum getDirection() {
    return this.direction;
  }

  public void setDirection(VehicleDirectionEnum direction) {
    this.direction = direction;
  }

  public long getFirstAxelTimestamp() {
    return this.firstAxelTimestamp;
  }

  public void setFirstAxelTimestamp(long firstAxelTimestamp) {
    this.firstAxelTimestamp = firstAxelTimestamp;
  }

  public long getSecondAxelTimestamp() {
    return this.secondAxelTimestamp;
  }

  public void setSecondAxelTimestamp(long secondAxelTimestamp) {
    this.secondAxelTimestamp = secondAxelTimestamp;
  }
  

}
