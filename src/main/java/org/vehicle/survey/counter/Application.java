package org.vehicle.survey.counter;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.analysis.impl.VehicleRecordAnalysisServiceImpl;
import org.vehicle.survey.counter.analysis.report.average.VehicleAverageCountPerHour;
import org.vehicle.survey.counter.analysis.report.count.VehicleCountMorningVsEveningReport;
import org.vehicle.survey.counter.analysis.report.count.VehicleCountPerFifteenMinutesReport;
import org.vehicle.survey.counter.analysis.report.count.VehicleCountPerHalfHourReport;
import org.vehicle.survey.counter.analysis.report.count.VehicleCountPerHourReport;
import org.vehicle.survey.counter.analysis.report.count.VehicleCountPerTwentyMinutesReport;
import org.vehicle.survey.counter.analysis.report.distance.AverageDistanceBetweenVehiclesReport;
import org.vehicle.survey.counter.analysis.report.distance.AverageDistanceBetweenVehiclesReportGenerator;
import org.vehicle.survey.counter.analysis.report.peak.PeakHourVolumeTimesReportGenerator;
import org.vehicle.survey.counter.analysis.report.speed.SpeedDistributionReportGeneratorByHourReport;
import org.vehicle.survey.counter.exception.VehicleRecordParserException;
import org.vehicle.survey.counter.parser.VehicleRecordParser;
import org.vehicle.survey.counter.utils.FileUtils;

/**
 * Class with main method
 */
public class Application {

  /**
   * the LOGGER
   */
  private static final Logger LOGGER = Logger.getLogger(VehicleRecordParser.class.getName());

  /**
   * the main method
   */
  public static void main(String[] args) {

    Application application = new Application();
    try {

      List<String> parsedStrings = null;

      if (args == null || args.length == 0) {
        //Consider - getting path in command line
        URI uri = application.getClass().getResource("/data.txt").toURI();
        initFileSystem(uri);
        Path path = Paths.get(uri);
        parsedStrings = FileUtils.readFileAsList(path);
      } else {
        parsedStrings = FileUtils.readFileAsList(args[0]);
      }

      if (parsedStrings == null || parsedStrings.isEmpty()) {
        LOGGER.severe(
            "Nothing to report, check the file path if you specified a file as a parameter");
        throw new IllegalStateException();
      }

      generateReports(parsedStrings);

    } catch (URISyntaxException e) {
      LOGGER.log(Level.SEVERE, "URISyntaxException caught, please check the file exists", e);
    } catch (VehicleRecordParserException e) {
      LOGGER.log(Level.SEVERE, "Unable to parse the file, check the contents of the file", e);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Unable to access the file", e);
    }


  }

  private static void generateReports(List<String> parsedStrings)
      throws VehicleRecordParserException {
    VehicleRecordParser vehicleRecordParser = new VehicleRecordParser();

    VehicleRecordAnalysisService vehicleRecordAnalysisService = new VehicleRecordAnalysisServiceImpl(
        vehicleRecordParser.parseDataFile(parsedStrings));

    //Count reports
    VehicleCountMorningVsEveningReport vehicleCountMorningVsEveningReport = new VehicleCountMorningVsEveningReport();
    vehicleCountMorningVsEveningReport.displayReport(vehicleRecordAnalysisService);

    VehicleCountPerHourReport vehicleCountPerHourReport = new VehicleCountPerHourReport();
    vehicleCountPerHourReport.displayReport(vehicleRecordAnalysisService);

    VehicleCountPerHalfHourReport vehicleCountPerHalfHourReport = new VehicleCountPerHalfHourReport();
    vehicleCountPerHalfHourReport.displayReport(vehicleRecordAnalysisService);

    VehicleCountPerTwentyMinutesReport vehicleCountPerHaCountPerTwentyMinutes = new VehicleCountPerTwentyMinutesReport();
    vehicleCountPerHaCountPerTwentyMinutes.displayReport(vehicleRecordAnalysisService);

    VehicleCountPerFifteenMinutesReport vehicleCountPerHFifteenMinutes = new VehicleCountPerFifteenMinutesReport();
    vehicleCountPerHFifteenMinutes.displayReport(vehicleRecordAnalysisService);

    //Average count
    VehicleAverageCountPerHour vehicleAverageCountPerHour = new VehicleAverageCountPerHour();
    vehicleAverageCountPerHour.displayReport(vehicleRecordAnalysisService);

    //Peak Volume times
    PeakHourVolumeTimesReportGenerator peakHourVolumeTimesReport = new PeakHourVolumeTimesReportGenerator();
    peakHourVolumeTimesReport.displayReport(vehicleRecordAnalysisService);

    //The (rough) speed distribution of traffic.
    SpeedDistributionReportGeneratorByHourReport speedDistributionByHourReport = new SpeedDistributionReportGeneratorByHourReport();
    speedDistributionByHourReport.displayReport(vehicleRecordAnalysisService);

    //Rough distance between cars during various periods.
    AverageDistanceBetweenVehiclesReportGenerator averageDistanceBetweenVehiclesReportGenerator = new AverageDistanceBetweenVehiclesReport();
    averageDistanceBetweenVehiclesReportGenerator.displayReport(vehicleRecordAnalysisService);
  }

  /**
   * initializes file system
   */
  private static FileSystem initFileSystem(URI uri) throws IOException {
    try {
      Map<String, String> env = new HashMap<>();
      return FileSystems.newFileSystem(uri, env);
    } catch (IllegalArgumentException | FileSystemNotFoundException e) {
      return FileSystems.getDefault();
    }
  }

}
