package org.vehicle.survey.counter.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.vehicle.survey.counter.config.ApplicationConfig;
import org.vehicle.survey.counter.exception.VehicleRecordParserException;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;
import org.vehicle.survey.counter.model.VehicleRecord;

/**
 * Class contains methods to parse the sensor readings
 */
public class VehicleRecordParser {

  /**
   * the LOGGER
   */
  private static final Logger LOGGER = Logger.getLogger(VehicleRecordParser.class.getName());

  private static final String INVALID_ENTRIES_ERROR_MESSAGE = "Invalid entries :";

  /**
   * The day for which record is being processed
   */
  private int currentDay;

  /**
   * The last processed record time
   */
  private long previousRecordTime;

  /**
   * Precompiled pattern - this can be made configurable
   */
  private final Pattern linePattern = Pattern.compile(ApplicationConfig.INPUT_VALIDATION_REGEX);

  /**
   * Returns a list of {@link VehicleRecord} for a given input list
   */
  public List<VehicleRecord> parseDataFile(List<String> inputlist)
      throws VehicleRecordParserException {

    List<VehicleRecord> vehicleRecords = new ArrayList<>();

    this.currentDay = 1;

    while (!inputlist.isEmpty()) {

      if (inputlist.size() < 2) {
        throw new VehicleRecordParserException("Not enough records to process");
      }

      // Assuming the records will be in sequence, if AA then its north bound
      VehicleDirectionEnum direction = this.findDirection(inputlist.get(0), inputlist.get(1));

      if (direction == VehicleDirectionEnum.B) {
        inputlist = this.processSouthBoundRecords(inputlist, vehicleRecords);
      } else {
        inputlist = this.processNorthBoundRecords(inputlist, vehicleRecords);
      }

    }

    return vehicleRecords;
  }


  private List<String> processSouthBoundRecords(List<String> input,
      List<VehicleRecord> vehicleRecords)
      throws VehicleRecordParserException {
    if (input.size() < 4) {
      throw new VehicleRecordParserException(
          "Less than four iputs, cant process southbound records");
    }
    Matcher sensorReadingMatcher1 = this.linePattern.matcher(input.get(0));
    Matcher sensorReadingMatcher2 = this.linePattern.matcher(input.get(1));
    Matcher sensorReadingMatcher3 = this.linePattern.matcher(input.get(2));
    Matcher sensorReadingMatcher4 = this.linePattern.matcher(input.get(3));

    if (sensorReadingMatcher1.matches() && sensorReadingMatcher2.matches() && sensorReadingMatcher3
        .matches() && sensorReadingMatcher4.matches()) {

      VehicleDirectionEnum sensorDirection1 = VehicleDirectionEnum
          .valueOf(sensorReadingMatcher1.group(1).toUpperCase().trim());
      VehicleDirectionEnum sensorDirection2 = VehicleDirectionEnum
          .valueOf(sensorReadingMatcher2.group(1).toUpperCase().trim());
      VehicleDirectionEnum sensorDirection3 = VehicleDirectionEnum
          .valueOf(sensorReadingMatcher3.group(1).toUpperCase().trim());
      VehicleDirectionEnum sensorDirection4 = VehicleDirectionEnum
          .valueOf(sensorReadingMatcher4.group(1).toUpperCase()
              .trim());

      if (!this
          .isSouthBoundSenorReadingsValid(sensorDirection1, sensorDirection2, sensorDirection3,
              sensorDirection4)) {
        LOGGER.log(Level.SEVERE, "Invalid sequence of entries");
        throw new VehicleRecordParserException(
            "Invalid sequence of entries : " + input.get(0) + "," + input.get(1) + ","
                + input.get(2) + "," + input.get(3));
      }

      long sensorReadingTimestamp1 = Long.parseLong(sensorReadingMatcher1.group(2));
      long sensorReadingTimestamp3 = Long.parseLong(sensorReadingMatcher3.group(2));

      VehicleRecord vehicleRecord = new VehicleRecord(this.getCurrentDay(sensorReadingTimestamp1),
          VehicleDirectionEnum.B, sensorReadingTimestamp1,
          sensorReadingTimestamp3);

      vehicleRecords.add(vehicleRecord);

    } else {
      LOGGER.log(Level.SEVERE,
          "Invalid entries : " + input.get(0) + "," + input.get(1) + "," + input.get(2) + ","
              + input.get(3));
      throw new VehicleRecordParserException(
          "Invalid entries : " + input.get(0) + "," + input.get(1) + "," + input.get(2) + ","
              + input.get(3));
    }

    return input.subList(4, input.size());
  }

  private boolean isSouthBoundSenorReadingsValid(VehicleDirectionEnum sensorDirection1,
      VehicleDirectionEnum sensorDirection2,
      VehicleDirectionEnum sensorDirection3, VehicleDirectionEnum sensorDirection4) {
    return sensorDirection1.equals(VehicleDirectionEnum.A)
        && sensorDirection2.equals(VehicleDirectionEnum.B)
        && sensorDirection3.equals(VehicleDirectionEnum.A)
        && sensorDirection4.equals(VehicleDirectionEnum.B);
  }

  private List<String> processNorthBoundRecords(List<String> input,
      List<VehicleRecord> vehicleRecords)
      throws VehicleRecordParserException {

    Matcher sensorReadingMatcher1 = this.linePattern.matcher(input.get(0));
    Matcher sensorReadingMatcher2 = this.linePattern.matcher(input.get(1));

    if (sensorReadingMatcher1.matches() && sensorReadingMatcher2.matches()) {

      long sensorReadingTimestamp1 = Long.parseLong(sensorReadingMatcher1.group(2));
      long sensorReadingTimestamp2 = Long.parseLong(sensorReadingMatcher2.group(2));

      VehicleRecord vehicleRecord = new VehicleRecord(this.getCurrentDay(sensorReadingTimestamp1),
          VehicleDirectionEnum.A, sensorReadingTimestamp1, sensorReadingTimestamp2);

      vehicleRecords.add(vehicleRecord);

    } else {
      throw new VehicleRecordParserException(
          INVALID_ENTRIES_ERROR_MESSAGE + " " + input.get(0) + "," + input.get(1));
    }

    return input.subList(2, input.size());
  }

  private int getCurrentDay(long currentTimestamp) {
    if (currentTimestamp < this.previousRecordTime) {
      this.currentDay++;
    }
    this.previousRecordTime = currentTimestamp;
    return this.currentDay;
  }

  private VehicleDirectionEnum findDirection(String firstEntry, String secondEntry) {
    if (firstEntry.startsWith(ApplicationConfig.SENSOR1_PREFIX) && secondEntry
        .startsWith(ApplicationConfig.SENSOR1_PREFIX)) {
      return VehicleDirectionEnum.A;
    }
    return VehicleDirectionEnum.B;
  }
}
