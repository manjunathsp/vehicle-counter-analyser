package org.vehicle.survey.counter.exception;

/**
 * Exception class for handling checked exceptions
 */
public class VehicleRecordParserException extends Exception {

  public VehicleRecordParserException(String message) {
    super(message);
  }
}
