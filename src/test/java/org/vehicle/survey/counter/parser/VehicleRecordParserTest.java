package org.vehicle.survey.counter.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.vehicle.survey.counter.exception.VehicleRecordParserException;
import org.vehicle.survey.counter.model.VehicleRecord;

class VehicleRecordParserTest {


  @Test
  public void shouldCreateTwoRecords() throws Exception {
    List<String> input = new ArrayList<>(
        Arrays.asList("A12345", "A123456", "A1234567", "A12345678"));
    List<VehicleRecord> output = new VehicleRecordParser().parseDataFile(input);

    assertEquals(2, output.size());
  }

  @Test
  public void shouldCreateOneRecordSoputhBound() throws Exception {
    List<String> input = new ArrayList<>(Arrays.asList("A12345", "B1236", "A1345", "B1356"));
    List<VehicleRecord> output = new VehicleRecordParser().parseDataFile(input);

    assertEquals(1, output.size());
  }

  @Test
  public void shouldCreateMultipleRecordsSouthbound() throws Exception {
    List<String> input = new ArrayList<>(
        Arrays.asList("A12345", "B1236", "A1345", "B1356", "A1357", "B1358", "A1359", "B1360"));
    List<VehicleRecord> output = new VehicleRecordParser().parseDataFile(input);

    assertEquals(2, output.size());
  }


  @Test
  public void shouldCreateMultipleEntriesInBothDirections() throws Exception {
    List<String> input = new ArrayList<>(
        Arrays.asList("A12345", "A123456", "A1345", "B1356", "A1357", "B1358", "A1359", "A1360"));
    List<VehicleRecord> output = new VehicleRecordParser().parseDataFile(input);

    assertEquals(3, output.size());
  }

  @Test
  public void shouldThrowExceptionForInsufficientNumberOfInputsInNorthDirection()
      throws Exception {
    List<String> input = new ArrayList<>(Arrays.asList("A12345", "A123456", "A1345"));

    assertThrows(VehicleRecordParserException.class,
        () -> new VehicleRecordParser().parseDataFile(input));
  }

  @Test
  public void shouldThrowExceptionForInsufficientNumberOfInputsInSouthDirection()
      throws Exception {
    List<String> input = new ArrayList<>(
        Arrays.asList("A12345", "B1236", "A1345", "B1236", "A1345", "B1236", "A1345"));

    assertThrows(VehicleRecordParserException.class,
        () -> new VehicleRecordParser().parseDataFile(input));
  }

  @Test
  public void shouldThrowExceptionForInsufficientNumberOfInputs() throws Exception {
    List<String> input = new ArrayList<>(Arrays.asList("A12345"));

    assertThrows(VehicleRecordParserException.class,
        () -> new VehicleRecordParser().parseDataFile(input));
  }

  @Test
  public void shouldThrowExceptionForInsufficientNumberOfISouthBoundInputs() throws Exception {
    List<String> input = new ArrayList<>(Arrays.asList("A12345", "B1236", "A1345", "A123456"));

    assertThrows(VehicleRecordParserException.class,
        () -> new VehicleRecordParser().parseDataFile(input));
  }

  @Test
  public void shouldThrowExceptionForInsufficientNumberOfISouthBoundInput2() throws Exception {
    List<String> input = new ArrayList<>(Arrays.asList("A12345", "B1236", "B1345", "A123456"));

    assertThrows(VehicleRecordParserException.class,
        () -> new VehicleRecordParser().parseDataFile(input));
  }


  @Test
  public void shouldIncrementDayIfOneEntryTimeIsLessThanPreviousEntryTimeOnNorthDirection()
      throws Exception {
    List<String> input = new ArrayList<>(Arrays.asList("A2000", "A2005", "A1000", "A1005"));
    List<VehicleRecord> output = new VehicleRecordParser().parseDataFile(input);

    assertEquals(1, output.get(0).getDay());
    assertEquals(2, output.get(1).getDay());
  }

  @Test
  public void shouldIncrementDayIfOneEntryTimeIsLessThanPreviousEntryTimeOnSouthDirection()
      throws Exception {
    List<String> input = new ArrayList<>(
        Arrays.asList("A2000", "B2005", "A2100", "B2105", "A1000", "B1005", "A1100", "B1105"));
    List<VehicleRecord> output = new VehicleRecordParser().parseDataFile(input);

    assertEquals(1, output.get(0).getDay());
    assertEquals(2, output.get(1).getDay());
  }

  @Test
  public void shouldNotIncrementDayIfSecondEntryTimeIsGreaterThanPreviousEntryTimeOnNorthDirection()
      throws Exception {
    List<String> input = new ArrayList<>(Arrays.asList("A2000", "A2005", "A3000", "A3005"));
    List<VehicleRecord> output = new VehicleRecordParser().parseDataFile(input);

    assertEquals(1, output.get(0).getDay());
    assertEquals(1, output.get(1).getDay());
  }

  @Test
  public void shouldNotIncrementDayIfSecondEntryTimeIsGreaterThanPreviousEntryTimeOnSouthDirection()
      throws Exception {
    List<String> input = new ArrayList<>(
        Arrays.asList("A2000", "B2005", "A2100", "B2105", "A3000", "B3005", "A3100", "B3105"));
    List<VehicleRecord> output = new VehicleRecordParser().parseDataFile(input);

    assertEquals(1, output.get(0).getDay());
    assertEquals(1, output.get(1).getDay());
  }

}