package org.vehicle.survey.counter.utils;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FileUtilsTest {

  @Test
  void readFileAsList() throws URISyntaxException {
    Path path = Paths.get(this.getClass().getClassLoader()
        .getResource("test_data.txt").toURI());
    List<String> fileContent = FileUtils.readFileAsList(path);
    Assertions.assertFalse(fileContent.isEmpty());
  }

  @Test
  void readFileAsListEmptyFile() throws URISyntaxException {
    Path path = Paths.get(this.getClass().getClassLoader()
        .getResource("test_data_empty.txt").toURI());
    List<String> fileContent = FileUtils.readFileAsList(path);
    Assertions.assertTrue(fileContent.isEmpty());
  }

  @Test
  void readFileAsListFileNotExixts() throws URISyntaxException {
    Path path = Paths.get("no_existing_file.txt");
    List<String> fileContent = FileUtils.readFileAsList(path);
    Assertions.assertTrue(fileContent.isEmpty());
  }

  @Test
  void readFileAsListFromFileSystem() throws URISyntaxException {
    Path path = Paths.get(this.getClass().getClassLoader()
        .getResource("test_data.txt").toURI());
    List<String> fileContent = FileUtils.readFileAsList(path.toAbsolutePath());
    Assertions.assertFalse(fileContent.isEmpty());
  }

  @Test
  void readFileAsListEmptyFileFromFileSystem() throws URISyntaxException {
    Path path = Paths.get(this.getClass().getClassLoader()
        .getResource("test_data_empty.txt").toURI());
    List<String> fileContent = FileUtils.readFileAsList(path.toAbsolutePath());
    Assertions.assertTrue(fileContent.isEmpty());
  }

}