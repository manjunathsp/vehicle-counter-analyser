package org.vehicle.survey.counter.analysis.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.vehicle.survey.counter.analysis.AverageCountResult;
import org.vehicle.survey.counter.analysis.PeakVolumeResult;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;
import org.vehicle.survey.counter.model.VehicleRecord;

class VehicleRecordAnalysisServiceImplTest {

  private static final List<VehicleRecord> vehicleRecords = new ArrayList<>();

  private VehicleRecordAnalysisService appUnderTest;

  @BeforeAll
  public static void setUp() throws Exception {
    vehicleRecords.add(new VehicleRecord(1, VehicleDirectionEnum.B, 8771, 8888));
    vehicleRecords.add(new VehicleRecord(1, VehicleDirectionEnum.A, 38771, 38888));
    vehicleRecords.add(new VehicleRecord(1, VehicleDirectionEnum.B, 338771, 338888));
    vehicleRecords.add(new VehicleRecord(1, VehicleDirectionEnum.A, 6338771, 6338888));
    vehicleRecords.add(new VehicleRecord(1, VehicleDirectionEnum.B, 86338771, 86338888));

    vehicleRecords.add(new VehicleRecord(2, VehicleDirectionEnum.A, 8633, 8733));
    vehicleRecords.add(new VehicleRecord(2, VehicleDirectionEnum.A, 86338, 86400));
    vehicleRecords.add(new VehicleRecord(2, VehicleDirectionEnum.A, 8633877, 8633388));
    vehicleRecords.add(new VehicleRecord(2, VehicleDirectionEnum.B, 8633877, 8633888));
    vehicleRecords.add(new VehicleRecord(2, VehicleDirectionEnum.A, 86338771, 86338888));
    vehicleRecords.add(new VehicleRecord(2, VehicleDirectionEnum.B, 87338888, 86400012));
  }

  @BeforeEach
  public void init() {
    this.appUnderTest = new VehicleRecordAnalysisServiceImpl(vehicleRecords);
  }

  @Test
  public void findVehicleCountByDireactionAndTimestamp() {
    Map<Integer, List<VehicleRecord>> vehicleCountMap = this.appUnderTest
        .findVehicleCount(VehicleDirectionEnum.A, 8000, 10000);

    assertEquals(1, vehicleCountMap.size());
  }


  @Test
  public void findVehicleCountByDirectionOnly() {
    Map<Integer, List<VehicleRecord>> vehicleCountMap = this.appUnderTest
        .findVehicleCount(VehicleDirectionEnum.A, 0, Duration.ofDays(1).toMillis());

    assertEquals(2, vehicleCountMap.get(1).size());
    assertEquals(4, vehicleCountMap.get(2).size());
  }

  @Test
  public void findPeakVolume() {
    PeakVolumeResult result = this.appUnderTest
        .findPeakVolume(Duration.ofMinutes(60).toMillis(), VehicleDirectionEnum.A);

    assertEquals(2, result.getDay());
    assertEquals(2, result.getNumber());
  }

  @Test
  public void getCountAveragesDirectionA() {
    Map<Long, AverageCountResult> result = this.appUnderTest
        .getCountAverages(Duration.ofDays(1).toMillis(), VehicleDirectionEnum.A);

    assertEquals(3.0, result.get(Long.valueOf(0)).getAverage());
  }

  @Test
  public void getCountAveragesDirectionB() {
    Map<Long, AverageCountResult> result = this.appUnderTest
        .getCountAverages(Duration.ofDays(1).toMillis(), VehicleDirectionEnum.B);

    assertEquals(2.0, result.get(Long.valueOf(0)).getAverage());
  }

  @Test
  public void getCountAveragesDirectionAInterval20() {
    Map<Long, AverageCountResult> result = this.appUnderTest
        .getCountAverages(Duration.ofMinutes(20).toMillis(), VehicleDirectionEnum.B);

    assertEquals(1.0, result.get(Long.valueOf(0)).getAverage());
    assertEquals(72, result.size());
  }


  @Test
  public void testGetSpeedAveragesDistribution() {
    Map<Long, AverageCountResult> result = this.appUnderTest
        .getSpeedAverages(Duration.ofMinutes(20).toMillis(), VehicleDirectionEnum.A);

    assertEquals(72, result.size());
  }


  @Test
  public void testGetSpeedAveragesAccuracyDirectionA() {

    List<VehicleRecord> vehicleRecordsTest = new ArrayList<>();

    vehicleRecordsTest.add(new VehicleRecord(1, VehicleDirectionEnum.A, 98186, 98333));
    vehicleRecordsTest.add(new VehicleRecord(1, VehicleDirectionEnum.A, 499718, 499886));

    VehicleRecordAnalysisService vehicleRecordAnalysisService = new VehicleRecordAnalysisServiceImpl(
        vehicleRecordsTest);

    final Map<Long, AverageCountResult> speedAverages = vehicleRecordAnalysisService
        .getSpeedAverages(Duration.ofDays(1).toMillis(), VehicleDirectionEnum.A);

    float average = speedAverages.get(Long.valueOf(0)).getAverage();
    assertEquals((float) 57.39796, average);
  }


  @Test
  public void testGetSpeedAveragesAccuracyDirectionB() {

    List<VehicleRecord> vehicleRecordsTest = new ArrayList<>();

    vehicleRecordsTest.add(new VehicleRecord(1, VehicleDirectionEnum.B, 638381, 638521));

    VehicleRecordAnalysisService vehicleRecordAnalysisService = new VehicleRecordAnalysisServiceImpl(
        vehicleRecordsTest);

    final Map<Long, AverageCountResult> speedAverages = vehicleRecordAnalysisService
        .getSpeedAverages(Duration.ofDays(1).toMillis(), VehicleDirectionEnum.B);

    float average = speedAverages.get(Long.valueOf(0)).getAverage();
    assertEquals((float) 64.28571, average);
  }

  @Test
  public void testGetSpeedAveragesIsZeroForNonExistentRecord() {

    List<VehicleRecord> vehicleRecordsTest = new ArrayList<>();

    vehicleRecordsTest.add(new VehicleRecord(1, VehicleDirectionEnum.A, 638381, 638521));

    VehicleRecordAnalysisService vehicleRecordAnalysisService = new VehicleRecordAnalysisServiceImpl(
        vehicleRecordsTest);

    final Map<Long, AverageCountResult> speedAverages = vehicleRecordAnalysisService
        .getSpeedAverages(Duration.ofDays(1).toMillis(), VehicleDirectionEnum.B);

    float average = speedAverages.get(Long.valueOf(0)).getAverage();
    assertEquals((float) 0.0, average);
  }

  @Test
  public void testGetDistanceAveragesDistribution() {
    Map<Long, AverageCountResult> result = this.appUnderTest
        .getDistanceAverages(Duration.ofMinutes(20).toMillis(), VehicleDirectionEnum.A);

    assertEquals(72, result.size());
  }


  @Test
  public void testGetDistanceAveragesAccuracyDirectionA() {

    List<VehicleRecord> vehicleRecordsTest = new ArrayList<>();

    vehicleRecordsTest.add(new VehicleRecord(1, VehicleDirectionEnum.A, 98186, 98333));
    vehicleRecordsTest.add(new VehicleRecord(1, VehicleDirectionEnum.A, 499718, 499886));

    VehicleRecordAnalysisService vehicleRecordAnalysisService = new VehicleRecordAnalysisServiceImpl(
        vehicleRecordsTest);

    final Map<Long, AverageCountResult> distanceAverages = vehicleRecordAnalysisService
        .getDistanceAverages(Duration.ofDays(1).toMillis(), VehicleDirectionEnum.A);

    float average = distanceAverages.get(Long.valueOf(0)).getAverage();
    assertEquals((float) 6692.2, average);
  }


  @Test
  public void testGetDistanceAccuracyDirectionB() {

    List<VehicleRecord> vehicleRecordsTest = new ArrayList<>();

    vehicleRecordsTest.add(new VehicleRecord(1, VehicleDirectionEnum.B, 638381, 638521));
    vehicleRecordsTest.add(new VehicleRecord(1, VehicleDirectionEnum.B, 638524, 638525));

    VehicleRecordAnalysisService vehicleRecordAnalysisService = new VehicleRecordAnalysisServiceImpl(
        vehicleRecordsTest);

    final Map<Long, AverageCountResult> speedAverages = vehicleRecordAnalysisService
        .getDistanceAverages(Duration.ofDays(1).toMillis(), VehicleDirectionEnum.B);

    float average = speedAverages.get(Long.valueOf(0)).getAverage();
    assertEquals((float) 2.3833334, average);
  }

  @Test
  public void testGetDistanceAveragesIsZeroForNonExistentRecord() {

    List<VehicleRecord> vehicleRecordsTest = new ArrayList<>();

    vehicleRecordsTest.add(new VehicleRecord(1, VehicleDirectionEnum.A, 638381, 638521));

    VehicleRecordAnalysisService vehicleRecordAnalysisService = new VehicleRecordAnalysisServiceImpl(
        vehicleRecordsTest);

    final Map<Long, AverageCountResult> speedAverages = vehicleRecordAnalysisService
        .getDistanceAverages(Duration.ofDays(1).toMillis(), VehicleDirectionEnum.B);

    float average = speedAverages.get(Long.valueOf(0)).getAverage();
    assertEquals((float) 0.0, average);
  }
}