package org.vehicle.survey.counter.analysis.report;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import org.junit.jupiter.api.Test;

class ReportGeneratorTest {

  @Test
  void getFormattedTime() {
    ReportGenerator reportGenerator = new TestClass();

    String formattedTime = reportGenerator.getFormattedTime(Duration.ofMinutes(45).toMillis());
    assertEquals("00:45:00", formattedTime);

    formattedTime = reportGenerator.getFormattedTime(Duration.ofDays(1).toMillis());
    assertEquals("24:00:00", formattedTime);

    formattedTime = reportGenerator.getFormattedTime(Duration.ofHours(45).toMillis());
    assertEquals("45:00:00", formattedTime);
  }


  private class TestClass implements ReportGenerator {

    @Override
    public String getReportTitle() {
      return "I am a test class";
    }
  }
}