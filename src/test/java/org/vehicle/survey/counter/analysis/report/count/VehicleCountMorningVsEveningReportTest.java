package org.vehicle.survey.counter.analysis.report.count;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.analysis.impl.VehicleRecordAnalysisServiceImpl;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;
import org.vehicle.survey.counter.model.VehicleRecord;

class VehicleCountMorningVsEveningReportTest {

  private final VehicleCountMorningVsEveningReport appUnderTest = new VehicleCountMorningVsEveningReport();

  private VehicleRecordAnalysisService vehicleRecordAnalysisService;

  private static final List<VehicleRecord> vehicleRecords = new ArrayList<>();

  private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private static final PrintStream originalOut = System.out;

  @BeforeAll
  public static void setUp() {

    System.setOut(new PrintStream(outContent));

    vehicleRecords.add(new VehicleRecord(1, VehicleDirectionEnum.A, 98186, 98333));
    vehicleRecords.add(new VehicleRecord(1, VehicleDirectionEnum.A, 499718, 499886));

  }

  @BeforeEach
  public void init() {
    this.vehicleRecordAnalysisService = new VehicleRecordAnalysisServiceImpl(vehicleRecords);
  }


  @Test
  void getReportTitleTest() {
    assertEquals("Total vehicle counts in each direction: morning versus evening",
        this.appUnderTest.getReportTitle());
  }

  @Test
  void displayReport() {
    this.appUnderTest.displayReport(this.vehicleRecordAnalysisService);
    //Contains day 1, 2 counts
    assertTrue(outContent.toString().contains("1\t\t\t 2\t\t\t 0"));
    //Shoud not contain day 5 or any other day except for 1
    assertFalse(outContent.toString().contains("5"));
  }

  @AfterAll()
  public static void tearDown() {
    System.setOut(originalOut);
  }
}