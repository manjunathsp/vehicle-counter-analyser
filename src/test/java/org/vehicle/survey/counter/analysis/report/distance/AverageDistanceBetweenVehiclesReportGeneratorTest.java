package org.vehicle.survey.counter.analysis.report.distance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.vehicle.survey.counter.analysis.VehicleRecordAnalysisService;
import org.vehicle.survey.counter.analysis.impl.VehicleRecordAnalysisServiceImpl;
import org.vehicle.survey.counter.model.VehicleDirectionEnum;
import org.vehicle.survey.counter.model.VehicleRecord;

class AverageDistanceBetweenVehiclesReportGeneratorTest {

  private final AverageDistanceBetweenVehiclesReportGenerator appUnderTest = new AverageDistanceBetweenVehiclesReport();

  private VehicleRecordAnalysisService vehicleRecordAnalysisService;

  private static final List<VehicleRecord> vehicleRecords = new ArrayList<>();

  private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private static final PrintStream originalOut = System.out;

  @BeforeAll
  public static void setUp() {

    System.setOut(new PrintStream(outContent));

    vehicleRecords.add(new VehicleRecord(1, VehicleDirectionEnum.A, 98186, 98333));
    vehicleRecords.add(new VehicleRecord(1, VehicleDirectionEnum.A, 499718, 499886));

  }

  @BeforeEach
  public void init() {
    this.vehicleRecordAnalysisService = new VehicleRecordAnalysisServiceImpl(vehicleRecords);
  }


  @Test
  void getIntervalTest() {
    assertEquals(3600000, this.appUnderTest.getInterval());
  }

  @Test
  void getReportTitleTest() {
    assertEquals("Rough distance between cars (in meters) during various periods",
        this.appUnderTest.getReportTitle());
  }

  @Test
  void displayReport() {
    this.appUnderTest.displayReport(this.vehicleRecordAnalysisService);
    assertTrue(outContent.toString().contains("00:00:00 \t\t\t\t\t6692.2\t\t\t\t\t\t\t\t0.0\t"));
  }

  @AfterAll()
  public static void tearDown() {
    System.setOut(originalOut);
  }
}